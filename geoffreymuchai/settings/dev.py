from .base import *


DEBUG = True
TEMPLATE_DEBUG = True
ENVIRONMENT = 'development'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    from .local import *
except ImportError:
    pass
