# Minimal requirements
Django>=1.7,<1.8
wagtail==0.8.7

# Recommended components (require additional setup):
psycopg2==2.5.2

# elasticsearch==1.1.1

# Recommended components to improve performance in production:
# django-redis-cache==0.13.0
# django-celery==3.1.10

# used during development for debugging and testing
ipdb==0.8
ipython==3.0.0
factory-boy==2.4.1

# Used during deployment
Fabric==1.10.0
gunicorn==19.1.1
gevent==0.13.6
