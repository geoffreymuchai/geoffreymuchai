import os
from fabric.api import (
    cd,
    env,
    prefix,
    put,
    run,
    sudo)
from fabric.colors import yellow

DEPLOYMENTS = {
    'prod': {
        'virtual_env': '/home/django/.virtualenvs/infemi',
        'project_dir': '/home/django/infemi',
        'host_string': 'django@infemi-beta.infemi.org',
    },
    'dev': {
        'virtual_env': '/home/vagrant/.virtualenvs/infemi',
        'project_dir': '/vagrant',
        'host_string': 'vagrant@192.168.33.10'
    }
}


def deploy(deployment="prod", branch="master"):
    env.update(DEPLOYMENTS[deployment])

    with cd(env.project_dir):
        with prefix('eval $(ssh-agent) && ssh-add ~/.ssh/id_rsa_bitbucket_wagtail'):
            run("git checkout {branch}".format(branch=branch))
            run("git pull origin {branch}".format(branch=branch))
        run('find . -name "*.pyc" -exec rm -rf {} \;')

        # run migrations
        with prefix('workon infemi'):
            run("pip install -r requirements.txt")
            run("python manage.py collectstatic --noinput")

            print(yellow("Syncing the database..."))
            run("python manage.py syncdb")

            print(yellow("Migrating the database..."))
            run("python manage.py migrate")

            print(yellow("Please enter the DJANGO user password..."))
            sudo('service infemi_beta_wagtail restart')

            # run("python setup.py test -q")
            # run("python setup.py install")
            # run("alembic -n {0} upgrade head".format(
            #     env.get('alembic_section', 'alembic')))
            # # Reload uWSGI
            # run("uwsgi --reload /var/run/infemi.pid")


def setup_env(deployment):
    env.update(DEPLOYMENTS[deployment])
    run('mkdir %s' % env.project_dir)

def make_virtual_envs(deployment="prod"):
    with cd(env.project_dir):
        run("mkvirtualenv infemi")


def server_setup(deployment="prod", dbuser='dbuser', dbpass="dbpwd"):
    setup_env(deployment)
    
    with cd(env.project_dir):
        with prefix('eval $(ssh-agent) && ssh-add ~/.ssh/id_rsa_bitbucket_wagtail'):
            run('git clone git@bitbucket.org:geoffreymuchai/infemi.git .'
                ' || (git fetch && git checkout master)')

    make_virtual_envs(deployment)

    put('config/nginx/nginx.conf',
        '/etc/nginx/sites-available/django', use_sudo=True)
    put('config/etc/infemi.conf',
        '/etc/init/infemi_beta_wagtail.conf', use_sudo=True)

    sudo('/etc/init.d/nginx reload')

    print "Server Setup Complete."
