# README #


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

**Summary of set up**

1.  Set up the python development environment using
`sudo apt-get install python-dev python-pip g++ libjpeg62-dev zlib1g-dev`

2.  Clone the repository through `git clone git@bitbucket.org:geoffreymuchai/geoffreymuchai.git`

**Configuration**

**Dependencies**

1. `pip install virtualenv`
2.  Create a virtual environment called `infemi`
3.  Install requirements listed in requirements.txt through `pip install -r requirements.txt`

** Database configuration**
`createdb -Upostgres geoffreymuchai`
`./manage.py migrate`
`./manage.py createsuperuser`

**How to run tests**

`./manage.py test`


**Deployment instructions**

Will be updated in due time.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Geoffrey Muchai

## Deployment

### Gotchas
- I ran into the following bug when starting ssh agent `could not open a connection to your authentication agent`.
[this post](http://stackoverflow.com/questions/17846529/could-not-open-a-connection-to-your-authentication-agent) suggested running `eval $(ssh-agent)` then `ssh-add your/private/key`.

- When building libsass I ran into the following error `x86_64-linux-gnu-gcc: internal compiler error: Killed (program cc1plus)`. [this site](https://bitcointalk.org/index.php?topic=304389.0) suggested increasing the swap space to 1GB using the following commands.
```
free
dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
mkswap /var/swap.img
swapon /var/swap.img
free
```

### References ###
http://wagtail.readthedocs.org/en/latest/getting_started/installation.html