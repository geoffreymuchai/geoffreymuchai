from django import template
from core.models import SMS

register = template.Library()

# SMS snippets


@register.inclusion_tag('core/tags/sms_list.html', takes_context=True)
def sms_list(context):
    return {
        'sms_list': SMS.objects.all(),
        'request': context['request'],
    }
