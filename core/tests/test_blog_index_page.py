# I got help on testing from the following sites:
# https://docs.djangoproject.com/en/1.7/topics/testing/overview/
# http://www.caktusgroup.com/blog/2013/07/17/factory-boy-alternative-django-testing-fixtures/

from django.test import TestCase
from core.tests.factories.blog_index_page_factory import BlogIndexPageFactory

class BlogIndexPageTestCase(TestCase):
    def test_blog_index_page_can_be_created(self):
        blog_index_page = BlogIndexPageFactory.create()
        self.assertEqual(blog_index_page.intro, 'Here comes the man dressed in blue')