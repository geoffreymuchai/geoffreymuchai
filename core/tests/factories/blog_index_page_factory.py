import factory
import random

from wagtail.wagtailcore.models import Page
from core.models.blog_index_page import BlogIndexPage
from core.models.home_page import HomePage
from django.contrib.contenttypes.models import ContentType

class BlogIndexPageFactory(factory.django.DjangoModelFactory):
	# Create content type for homepage model
	homepage_content_type, created = ContentType.objects.get_or_create(
								        model='homepage', 
								        app_label='core', 
								        defaults={'name': 'Homepage'}
								    )

	# Create a new homepage
	path = '000100%d0' % random.randint(1,9)
	homepage = HomePage.objects.create(
	    title="Homepage",
	    slug='home_yeah',
	    content_type=homepage_content_type,
	    path=path,
	    depth=2,
	    numchild=0,
	    url_path='/home_yeah/'
	)

	class Meta:
		model = BlogIndexPage

	page_ptr_id = homepage
	intro = "Here comes the man dressed in blue"