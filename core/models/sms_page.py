import requests

from django.db import models
from django.db.models.signals import post_save
from django.conf import settings

from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailsnippets.models import register_snippet


@register_snippet
class SMS(models.Model):
    phone_numbers = models.CharField(null=True, blank=True, max_length=1000)
    text = models.CharField(max_length=160)
    note = models.CharField(max_length=255)

    panels = [
        FieldPanel('phone_numbers'),
        FieldPanel('text'),
        FieldPanel('note'),
    ]

    def __unicode__(self):
        return "{}".format(self.text)


def send_sms(sender, instance, **kwargs):
    payload = {
        'username': settings.ROUTESMS_USERNAME,
        'password': settings.ROUTESMS_PASSWORD,
        'type': settings.ROUTESMS_TEXT_MESSAGE,
        'source': settings.ROUTESMS_SOURCE_PHONE_NUMBER,
        'dlr': 1,  # Delivery reports required
        'destination': instance.phone_numbers,
        'message': instance.text
    }
    url = 'http://smpp2.routesms.com:8080/bulksms/bulksms'

    response = requests.get(url, params=payload)

    print "Response Code: {}".format(response.status_code)


# register the signal
post_save.connect(send_sms,
                  sender=SMS,
                  dispatch_uid="dispatch_sms")
