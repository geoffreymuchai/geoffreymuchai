from django.db import models
from core.models.link_fields import LinkFields
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel


# Related links
class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")

    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

    class Meta:
        abstract = True
