from core.models.related_link import RelatedLink
from wagtail.wagtailcore.models import Orderable
from modelcluster.fields import ParentalKey

# Blog index page


class BlogIndexPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('BlogIndexPage', related_name='related_links')
